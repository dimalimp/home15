// 1-------------------------
// функция

function hi() {
    console.log("Привет");
}
hi();

// 2--------------------------
// функция с внешней переменной

const name = "Dima";

function hii() {
    console.log(`Привет ${name}`);
}
hii();

// 3---------------------------
// функция с параметром

function hiii(name) {
    console.log("Привет " + name);
}
hiii("Дима");

// 4----------------------------
// возвращает сумму трех чисел

function calcSum(a, b, c) {
    console.log(a + b);
    return a + b + c;
}
const sum = calcSum(3, 5, 7);
console.log(sum);

// 5----------------------------
// передача чисел параметрам

function getSum(a, b = 1, c) {
    if (a === undefined) {
        console.log("упс");
        return;
  }
    if (c === undefined) {
        return a * b;
  }
  return a + b + c;
}
const result = getSum(5, 5, 5);
console.log(result);

// 6------------------------------
// функция больше меньше

function minMax(num) {
    if (num === 10) {
        return;
    }
    if (num > 10) {
       return 20;
    } else if (num < 10) {
       console.log("вау");
       return;
    }
}
minMax(9)

// 7-------------------------------
// новый объект с новым полем

const obj = {
    age: 30,
    user: "Dima"
}

function newObj(b) {
    return {...b, a: 5};
}
newObj(obj);
console.log(newObj(obj))

// 8---------------------------------
// функция в объекте

function bn() {
    return "Ка" + "тя";
}
const nameUser = {
    one: "Ксюша",
    two: "Даша",
    three: bn(),
}
console.log(nameUser);

// 9---------------------------------
// функция в функции

function hiiii() {
    hi()
}
hiiii()

// 10--------------------------------
// если булевое правда или ложь

function a(str) {
    console.log(`privet ${str}`);
  }
  function b(str) {
    console.log(`poka ${str}`);
  }

  function c(bool, str, a, b) {
    if (!str || 0 === str.length) {
      return a;
    }
    bool ? a(str) : b(str);
  }

// 11----------------------------------
// возвращает количество параметров

function fa(a, b, c) { console.log(arguments.length) }
fa(4, 5, 6);