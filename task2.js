let arr = [{name: "igor", age: 23}, {name: "dima", age: 13}];

// 1--------------------------------
// сортирует от старшего к младшему SORT

function compare(a, b) {
    if (a.age > b.age) return 1;
    if (a.age == b.age) return 0;
    if (a.age < b.age) return -1;
  }

arr.sort(compare);

console.log(arr);

// 2--------------------------------
// возвращает элемент массива который равен строке FIND

function getFiltred (array, name) {
  return array.find(function (item) {
      return item.name === name;
  })
}
console.log(getFiltred(arr, 'igor'));

// 3----------------------------------
// возвращает новый массив который равен строке FILTER

function getFiltred (array, name) {
  return array.filter(function (item) {
      return item.name === name;
  })
}
console.log(getFiltred(arr, 'dima'));

// 4----------------------------------
// выводит в консоль все элементы массива FOREACH

arr.forEach(function(item) {
    console.log(item);
})

// 5---------------------------------
// новый массив и увеличивает на 1 MAP

const newArr = arr.map(function (item) {
  return {...item, age: item.age >= 50 ? item.age : item.age + 1}
})
console.log(newArr);

// 6------------------------------------
// REDUCE

let arrThree = [5, 6, 3, 0, 'aw', 'sds', 2];
const arrFour = arrThree.reduce(function (accumulator, currentValue, index) {
    if (!index) return [...accumulator] // это что бы без первого
    return typeof currentValue === 'number' ? [...accumulator, currentValue + index ] : [...accumulator, currentValue + currentValue ]
},[])
console.log(arrFour);